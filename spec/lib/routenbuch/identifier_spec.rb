require 'rails_helper'
require 'routenbuch/identifier'

RSpec.describe Routenbuch::Identifier do
  5.times do
    value = rand(999999999)

    it "encode/decode #{value}" do
      encoded_value = nil
      expect {
        encoded_value = Routenbuch::Identifier.encode(value)
      }.not_to raise_error
      decoded_value = nil
      expect {
        decoded_value = Routenbuch::Identifier.decode(encoded_value)
      }.not_to raise_error
      expect(decoded_value).to eq value
    end
  end
end
