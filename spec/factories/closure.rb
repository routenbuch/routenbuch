FactoryBot.define do
  factory :closure do
    name { 'No climbing here' }
    description { 'No climing between routes Saftpresse and Dampfhammer. No climbing when fullmoon.' }
    geo_ref factory: :region
    regular_start_month { 1 }
    regular_start_day_of_month { 1 }
    regular_end_month { 6 }
    regular_end_day_of_month { 31 }
    kind { 'flexible' }

    factory :closure_with_crags_and_routes do
      after(:create) do |object, evaluator|
        object.geo_refs << create_list(:crag, 3)
        object.routes << create_list(:route, 3)
      end
    end

    factory :closure_incomplete do
      kind { 'temporary' }
      regular_start_month { nil }
      regular_start_day_of_month { nil }
      regular_end_month { nil }
      regular_end_day_of_month { nil }

      factory :closure_only_season do
        after(:create) do |object, evaluator|
          create(:season_closure, closure: object)
        end
      end
    end

    factory :closure_multiyear do
      kind { 'fixed' }
      regular_start_month { 10 }
      regular_start_day_of_month { 1 }
      regular_end_month { 3 }
      regular_end_day_of_month { 31 }
    end
  end
end
