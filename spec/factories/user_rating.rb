FactoryBot.define do
  factory :user_rating do
    rating
    user
    score { 5 }
  end
end
