FactoryBot.define do
  factory :photo, aliases: [:geo_ref_photo] do
    target factory: :crag
    geo_ref do
      target.is_a?(GeoRef) ? target : target.geo_ref
    end
    description { 'cat content' }
    # causes IntegryError?!
    # photo do
    #   Rack::Test::UploadedFile.new(
    #     Rails.root.join('spec/fixtures/files/dummy.png'),
    #     'image/png',
    #     true
    #   )
    # end
    user

    after(:build) do |p|
      p.photo.attach(
        io: File.open(Rails.root.join('spec/fixtures/files/dummy.png')),
        filename: 'dummy.png',
        content_type: 'image/png'
      )
    end

    factory :route_photo, parent: :photo do
      target factory: :route
    end
  end
end
