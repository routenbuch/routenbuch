# frozen_string_literal: true

require 'rails_helper'

include JsonapiHelpers

RSpec.configure do |config|
  config.openapi_root = Rails.root.join('openapi').to_s

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:swaggerize' rake task, the complete Swagger will
  # be generated at the provided relative path under openapi_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.openapi_specs = {
    'embed/v1/openapi.yaml' => {
      openapi: '3.0.1',
      info: {
        title: 'Embedding API V1',
        version: 'v1'
      },
      paths: {},
      servers: [
        {
          url: '/',
        }
      ],
      components: {
        securitySchemes: {
          basic_auth: {
            type: :http,
            scheme: :basic
          },
        },
        schemas: {
          geo_ref: jsonapi_object(
            type: :object,
            properties: {
              name: { type: :string },
              description: { type: :string, nullable: true },
              alternative_names: {
                type: :array,
                items: { type: :string }
              },
            },
            required: [ 'name' ]
          ),
          closure: jsonapi_object(
            type: :object,
            properties: {
              name: { type: :string },
              description: { type: :string },
              region: {
                type: :array,
                items: { type: :string }
              },
              reason: { type: :string, nullable: true },
              kind: { type: :string },
              start_at: { type: :string, format: :date, nullable: true },
              end_at: { type: :string, format: :date, nullable: true },
              regular_start_at: { type: :string, format: :date, nullable: true },
              regular_end_at: { type: :string, format: :date, nullable: true },
              active: { type: :boolean },
              active_changed_at: { type: :string, format: :date, nullable: true },
              season_description: { type: :string, nullable: true },
              geo_refs: {
                type: :array,
                items: {
                  type: :object,
                  properties: {
                    id: { type: :string },
                    type: { type: :string },
                    name: { type: :string },
                    alternative_names: {
                      type: :array,
                      items: { type: :string }
                    },
                    route_first: { type: :string, nullable: true },
                    route_last: { type: :string, nullable: true },
                  }
                }
              },
              routes: {
                type: :array,
                items: {
                  type: :object,
                  properties: {
                    id: { type: :string },
                    name: { type: :string },
                    alternative_names: {
                      type: :array,
                      items: { type: :string }
                    },
                  }
                }
              }
            }
          ),
          jsonapi_collection_meta: {
            type: :object,
            properties: {
              total: { type: :integer }
            }
          },
          jsonapi_collection_links: {
            type: :object,
            properties: {
              self: { type: :string },
              next: { type: :string },
              prev: { type: :string },
            },
            required: [ 'self' ]
          }
        }
      }
    }
  }

  config.openapi_format = :yaml
end
