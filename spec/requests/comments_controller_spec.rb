require 'rails_helper'

RSpec.describe CommentsController, type: :request do
  with_user [:admin, :contributor, :user] do
    let(:route_comment) do
      create(:route_comment, user: @user)
    end
    let(:geo_ref_comment) do
      create(:geo_ref_comment, user: @user)
    end

    it 'index' do
      get comments_path
      expect(response).to have_http_status(:ok)
    end

    it 'index on geo_ref' do
      get geo_ref_comments_path(geo_ref_comment.target)
      expect(response).to have_http_status(:ok)
    end

    it 'index on route' do
      get route_comments_path(route_comment.target)
      expect(response).to have_http_status(:ok)
    end

    it 'new geo_ref comment' do
      get new_geo_ref_comment_path(geo_ref_comment.target)
      expect(response).to have_http_status(:ok)
    end

    it 'new route comment' do
      get new_route_comment_path(route_comment.target)
      expect(response).to have_http_status(:ok)
    end

    it 'create geo_ref comment' do
      post(
        geo_ref_comments_path(geo_ref_comment.target),
        params: {
          comment: {
            body: 'test comment'
          }
        }
      )
      expect(response).to have_http_status(:found)
      follow_redirect!
      expect(response.body).to include("Comment was successfully created.")
    end

    it 'create route comment' do
      post(
        route_comments_path(route_comment.target),
        params: {
          comment: {
            body: 'test comment'
          }
        }
      )
      expect(response).to have_http_status(:found)
      follow_redirect!
      expect(response.body).to include("Comment was successfully created.")
    end

    it 'edit' do
      get edit_comment_path(route_comment)
      expect(response).to have_http_status(:ok)
    end

    it 'update' do
      patch(
        comment_path(route_comment),
        params: {
          comment: {
            body: 'update me!'
          }
        }
      )
      expect(response).to have_http_status(:found)
      route_comment.reload
      expect(route_comment.body).to eq 'update me!'
      follow_redirect!
      expect(response.body).to include("Comment was successfully updated.")
    end

    it 'destroy' do
      delete comment_path(route_comment)
      expect(response).to have_http_status(:found)
      follow_redirect!
      expect(response.body).to include("Comment was successfully removed.")
      route_comment.reload
      expect(route_comment.deleted?).to be true
      expect(route_comment.body).to be_blank
    end

    context 'with soft-deleted comments' do
      before do
        route_comment.soft_delete!
        geo_ref_comment.soft_delete!
      end

      it 'index' do
        get comments_path
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
