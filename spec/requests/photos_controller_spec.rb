require 'rails_helper'

RSpec.describe PhotosController, type: :request do
  login_user :admin_user

  before(:each) do
    @route_photo = create(:route_photo, user: @user)
    @geo_ref_photo = create(:geo_ref_photo, user: @user)
  end

  it 'index' do
    get photos_path
    expect(response).to have_http_status(:ok)
  end

  it 'index on geo_ref' do
    get geo_ref_photos_path(@geo_ref_photo.target)
    expect(response).to have_http_status(:ok)
  end

  it 'index on route' do
    get route_photos_path(@route_photo.target)
    expect(response).to have_http_status(:ok)
  end

  it 'show' do
    get photo_path(@route_photo)
    expect(response).to have_http_status(:ok)
  end

  it 'new geo_ref photo' do
    get new_geo_ref_photo_path(@geo_ref_photo.target)
    expect(response).to have_http_status(:ok)
  end

  it 'new route photo' do
    get new_route_photo_path(@route_photo.target)
    expect(response).to have_http_status(:ok)
  end

  it 'create geo_ref photo' do
    post(
      geo_ref_photos_path(@geo_ref_photo.target),
      params: {
        photo: {
          description: 'test photo',
          photo: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/dummy.png'), 'image/png', true)
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Photo was successfully created.")
  end

  it 'create route photo' do
    post(
      route_photos_path(@route_photo.target),
      params: {
        photo: {
          description: 'test photo',
          photo: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/dummy.png'), 'image/png', true)
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Photo was successfully created.")
  end

  it 'edit' do
    get edit_photo_path(@route_photo)
    expect(response).to have_http_status(:ok)
  end

  it 'update' do
    patch(
      photo_path(@route_photo),
      params: {
        photo: {
          description: 'updated!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    @route_photo.reload
    expect(@route_photo.description).to eq 'updated!'
    follow_redirect!
    expect(response.body).to include("Photo was successfully updated.")
  end

  it 'destroy' do
    delete photo_path(@route_photo)
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Photo was successfully destroyed.")
    expect(Photo.exists? @route_photo.id).to be false
  end
end
