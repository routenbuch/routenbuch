require 'swagger_helper'

RSpec.describe 'api/embed/v1', type: :request do
  let(:user_factory) { :admin_user }
  let(:user) { create(user_factory) }
  let(:Authorization) do
    ActionController::HttpAuthentication::Basic.encode_credentials(
      user.email, user.password
    )
  end

  before(:each) do
    @geo_ref = create(:region)
    create(:closure_with_crags_and_routes, geo_ref: @geo_ref)
    create(:closure_incomplete, geo_ref: @geo_ref)
    create(:closure_only_season, geo_ref: @geo_ref)
    create(:closure_multiyear, geo_ref: @geo_ref)
  end

  after do |example|
    example.metadata[:response][:content] = {
      'application/json' => {
        example: JSON.parse(response.body, symbolize_names: true)
      }
    }
  end

  path '/api/embed/v1/closures' do
    get('retrieve closures') do
      tags 'Closures'
      security [ basic_auth: [] ]
      produces 'application/json'

      response(200, 'successful') do
        schema(
          jsonapi_collection(
            '$ref': '#/components/schemas/closure'
          )
        )
        run_test!
      end
    end
  end

  path '/api/embed/v1/closures/feed' do
    get('retrieve closures as feed') do
      tags 'Closures'
      security [ basic_auth: [] ]
      produces 'application/json'

      response(200, 'successful') do
        schema(
          jsonapi_collection(
            '$ref': '#/components/schemas/closure'
          )
        )
        run_test!
      end
    end
  end

  path '/api/embed/v1/geo_refs/{geo_ref_id}/closures' do
    get('retrieve closures of region') do
      tags 'Closures'
      security [ basic_auth: [] ]
      produces 'application/json'
      parameter name: :geo_ref_id, in: :path, type: :string

      let(:geo_ref_id) { @geo_ref.id }

      response(200, 'successful') do
        schema(
          jsonapi_collection(
            '$ref': '#/components/schemas/closure'
          )
        )
        run_test!
      end
    end
  end

  path '/api/embed/v1/geo_refs/{geo_ref_id}/closures/feed' do
    get('retrieve closures of region as feed') do
      tags 'Closures'
      security [ basic_auth: [] ]
      produces 'application/json'
      parameter name: :geo_ref_id, in: :path, type: :string

      let(:geo_ref_id) { @geo_ref.id }

      response(200, 'successful') do
        schema(
          jsonapi_collection(
            '$ref': '#/components/schemas/closure'
          )
        )
        run_test!
      end
    end
  end
end
