# == Schema Information
#
# Table name: comments
#
#  id          :bigint           not null, primary key
#  body        :text             default(""), not null
#  deleted_at  :datetime
#  kind        :string           default("comment"), not null
#  pinned      :boolean          default(FALSE), not null
#  private     :boolean          default(FALSE), not null
#  target_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  geo_ref_id  :bigint           not null
#  target_id   :integer
#  user_id     :bigint
#
# Indexes
#
#  index_comments_on_geo_ref_id                 (geo_ref_id)
#  index_comments_on_kind                       (kind)
#  index_comments_on_pinned                     (pinned)
#  index_comments_on_private                    (private)
#  index_comments_on_target_type_and_target_id  (target_type,target_id)
#  index_comments_on_user_id                    (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#
require 'rails_helper'

RSpec.describe Comment, type: :model do
  let(:attributes) { {} }
  let(:factory) { :comment }
  let(:comment) { create factory, **attributes }

  context 'object creation' do
    it 'validation' do
      expect { comment.validate! }.not_to raise_error
    end
  end

  context 'soft deletion' do
    before do
      comment.soft_delete
    end

    it 'soft deleted comment' do
      expect(comment.body).to be_blank
    end
  end
end
