# == Schema Information
#
# Table name: rbids
#
#  id         :bigint           not null, primary key
#  hint       :string           default(""), not null
#  item_type  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  item_id    :bigint           not null
#
# Indexes
#
#  index_rbids_on_item  (item_type,item_id)
#
require 'rails_helper'

RSpec.describe Rbid, type: :model do
  context 'rbid' do
    let(:rbid) { create :rbid }

    it 'validation' do
      expect { rbid.validate! }.not_to raise_error
    end

    it 'id' do
      expect(rbid.rbid).to be_a String
    end

    it 'item' do
      expect(rbid.item).to be_a ActiveRecord::Base
    end
  end
end
