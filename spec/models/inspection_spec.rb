# == Schema Information
#
# Table name: inspections
#
#  id                   :bigint           not null, primary key
#  body                 :text
#  when                 :date
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  regulated_geo_ref_id :bigint
#
# Indexes
#
#  index_inspections_on_regulated_geo_ref_id  (regulated_geo_ref_id)
#
require 'rails_helper'

RSpec.describe Inspection, type: :model do
  let(:attributes) { {} }
  let(:inspection) { create :inspection, **attributes }

  context 'inspection' do
    it 'validation' do
      expect { inspection.validate! }.not_to raise_error
    end

    it 'associations' do
      expect(inspection.regulated_geo_ref).to be_a(RegulatedGeoRef)
    end

    context 'without when' do
      let(:attributes) { { when: '' } }

      it 'validation' do
        expect { inspection.validate! }.to raise_error ActiveRecord::RecordInvalid
      end
    end
  end
end
