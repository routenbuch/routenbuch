# == Schema Information
#
# Table name: regulated_geo_refs
#
#  id            :bigint           not null, primary key
#  reason        :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  geo_ref_id    :bigint
#  regulation_id :bigint
#
# Indexes
#
#  index_regulated_geo_refs_on_geo_ref_id                    (geo_ref_id)
#  index_regulated_geo_refs_on_regulation_id                 (regulation_id)
#  index_regulated_geo_refs_on_regulation_id_and_geo_ref_id  (regulation_id,geo_ref_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#  fk_rails_...  (regulation_id => regulations.id)
#
require 'rails_helper'

RSpec.describe RegulatedGeoRef, type: :model do
  let(:geo_ref_type) { :crag }
  let(:reason) { '' }
  let(:attributes) do
    {
      geo_ref: create(geo_ref_type),
      reason: reason
    }
  end
  let(:regulated_geo_ref) { create :regulated_geo_ref, **attributes }

  context 'regulated_geo_ref' do
    it 'validation' do
      expect { regulated_geo_ref.validate! }.not_to raise_error
    end

    it 'associations' do
      expect(regulated_geo_ref.geo_ref).to be_a GeoRef
      expect(regulated_geo_ref.regulation).to be_a Regulation
      expect(regulated_geo_ref.inspections).to all(be_a(Inspection))
    end

    context 'with reason' do
      let(:reason) { 'Darum!' }

      it 'validation' do
        expect { regulated_geo_ref.validate! }.not_to raise_error
      end
    end

    context 'with geo_ref of type sector' do
      let(:geo_ref_type) { :sector }

      it 'validation' do
        expect { regulated_geo_ref.validate! }.not_to raise_error
      end
    end

    %i[country region].each do |type|
      context "with geo_ref of type #{type}" do
        let(:geo_ref_type) { type }

        it 'validation' do
          expect { regulated_geo_ref.validate! }.to raise_error /must be a crag or sector/
        end
      end
    end
  end
end
