# Maintenance

## Adding or updating Ruby Gems

Edit the Gemfile file and bump version numbers or add ndw dependencies. After
that run

```sh
podman-compose -p routenbuch exec web bundle update
```

which will update the Gemfile.lock without installing gems.

## Adding or updating nodejs packages

Edit the package.json file and bump version number or add new dependencies.
After that run

```sh
podman-compose -p routenbuch exec webpacker gosu app:app yarn install --production=false
```

which will update the yarn.lock file. If you want to audit the files, run

```sh
podman-compose -p routenbuch exec webpacker gosu app:app yarn audit
```
