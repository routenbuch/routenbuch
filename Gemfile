source 'https://rubygems.org'

gem 'rails', '~> 7.1.5'

gem 'net-smtp', require: false

gem 'bootsnap', require: false

gem 'pg'
gem 'pg_search'

gem 'puma'
gem 'seedbank'

gem 'sassc-rails'
gem 'shakapacker', '6.6.0'

gem 'simple_form'
gem 'simple-navigation'
gem 'simple_navigation_bootstrap'

# Use ActiveModel has_secure_password
gem 'bcrypt'
gem 'diff-lcs'
gem 'jsonb_accessor'
gem 'paper_trail'
gem 'strong_password'

# provide a health_check endpoint for monitoring
gem 'health_check'

gem 'cancancan'
gem 'devise'
gem 'devise-i18n'
gem 'kaminari'
gem 'ransack'
gem 'redcarpet'
gem 'soft_deletion'

# uploads
gem 'active_storage_validations'
gem 'image_processing'

gem 'redis'
gem 'sidekiq'
gem 'sidekiq-cron'

# for geo coding
gem 'geokit-rails'

gem 'jsonapi-serializer'

gem 'gettext_i18n_rails'
gem 'lograge'

gem 'rqrcode', '~> 2.0'

gem 'rswag-api'
gem 'rswag-ui'

group :development do
  gem 'annotate'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'bundle-audit'
  gem 'byebug'
  gem 'gettext', '>=3.0.2', require: false
  gem 'listen'
  gem 'rails-erd'
  gem 'spring'
  gem 'web-console'
  gem 'yard'
end

group :development, :test do
  gem 'database_cleaner-active_record'
  gem 'factory_bot_rails'
  gem 'rspec-rails'
  gem 'rswag-specs'
  gem 'simplecov', require: false
end
