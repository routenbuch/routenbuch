# configure I18n module used by rails and most gems
I18n.available_locales = Routenbuch.locales
I18n.default_locale = Routenbuch.default_locale

# configure gettext used for localization of application strings'),
FastGettext.add_text_domain 'app', :path => 'locale', :type => :po
FastGettext.default_available_locales = Routenbuch.locales.map { |l| l.to_s.gsub('-','_') }
FastGettext.default_text_domain = 'app'

# mark translations as safe strings
GettextI18nRails.translations_are_html_safe = true
