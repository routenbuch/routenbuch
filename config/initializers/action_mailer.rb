# configure action mailer settings

Rails.application.config.action_mailer.delivery_method = Routenbuch.config.mail.delivery_method.to_sym
ActionMailer::Base.delivery_method = Routenbuch.config.mail.delivery_method.to_sym

Rails.application.config.action_mailer.send(
  "#{Routenbuch.config.mail.delivery_method}_settings=",
  Routenbuch.config.mail.settings.symbolize_keys
)
ActionMailer::Base.send(
  "#{Routenbuch.config.mail.delivery_method}_settings=",
  Routenbuch.config.mail.settings.symbolize_keys
)

Rails.application.config.action_mailer.default_url_options = Routenbuch.config.base_url.symbolize_keys
ActionMailer::Base.default_url_options = Routenbuch.config.base_url.symbolize_keys
