require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Routenbuch
  class Application < Rails::Application
    config.assets.enabled = true
    config.assets.paths << Rails.root.join('node_modules')

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    config.action_dispatch.cookies_same_site_protection = :strict

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.exceptions_app = routes

    config.active_storage.variant_processor = :vips
    config.active_job.queue_adapter = :sidekiq

    config.logger = ActiveSupport::Logger.new(STDOUT)
  end
end
