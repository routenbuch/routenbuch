module Api
  module Embed
    module V1
      class GeoRefsController < Api::Embed::BaseController
        skip_authorization_check only: %i[with_closures]

        def with_closures
          @geo_refs = GeoRef.where(
            id: Closure.distinct.pluck(:geo_ref_id)
          ).accessible_by(current_ability, :read) \
            .page(params[:page])
          render json: GeoRefSerializer.new(
            @geo_refs,
            links: jsonapi_paginate_links(@geo_refs),
            meta: jsonapi_paginate_meta(@geo_refs),
          ).serializable_hash.to_json
        end
      end
    end
  end
end
