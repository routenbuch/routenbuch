class RoutesController < ApplicationController
  before_action :set_route, only: [:show, :edit, :update, :destroy]
  before_action :set_base, only: %i[index search autocomplete new create]
  skip_authorization_check :only => %i[index search autocomplete]

  include AdvancedSearchableController

  def autocomplete
    @routes =
      @base_relation
      .where('name ILIKE (?)', "%#{params[:term]}%")
      .includes(:geo_ref)
      .includes(:grade)
      .accessible_by(current_ability, :read)
      .order('LENGTH(routes.name) ASC, routes.name ASC')
      .limit(10)

    render(
      json: @routes.map do |r|
        {
          id: r.id,
          value: r.name,
          label: "#{r.geo_ref.name} / #{r.name} #{r.grade&.grade}"
        }
      end
    )
  end

  def index
    @q = @base_relation.ransack(params[:q])
    @routes =
      @q
      .result
      .includes(:grade, geo_ref: :path_cache)
      .accessible_by(current_ability, :read)
      .order(created_at: :desc)
      .page(params[:page])

    respond_to do |format|
      format.html
      format.json { render json: @routes.to_json(include: %i[geo_ref grade]) }
    end
  end

  def show
    authorize! :read, @route
    @route_links_from = @route.route_links_from \
      .includes(:route_to) \
      .accessible_by(current_ability, :read)
    @route_links_to = @route.route_links_to \
      .includes(:route_to) \
      .accessible_by(current_ability, :read)
    @rbids = @route.rbids
  end

  def new
    @route = Route.new
    @route.geo_ref = @base_object
    authorize! :create, @route
  end

  def edit
    authorize! :update, @route
  end

  def create
    @route = Route.new(route_params)
    @route.geo_ref = @base_object
    authorize! :create, @route

    if @route.save
      redirect_to @route, notice: _('Route was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @route
    if @route.update(route_params)
      redirect_to @route, notice: _('Route was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @route
    @route.destroy
    redirect_to geo_ref_path(@route.geo_ref), notice: _('Route was successfully destroyed.')
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [geo_ref, geo_ref.descendent_routes]
      elsif params[:first_ascent_person_id]
        first_ascent_person = FirstAscentPerson.find(params[:first_ascent_person_id])
        [first_ascent_person, first_ascent_person.routes]
      else
        [nil, Route]
      end
    authorize! :read, @base_object unless @base_object.nil?
  end

  def set_route
    @route = Route.find(params[:id])
  end

  def route_params
    p = params.require(:route).permit(
      :name,
      :body,
      :grade_id,
      :first_ascent_person_id,
      :first_ascent_year,
      :first_ascent_month,
      :first_ascent_day,
      :description,
      :pitches,
      :legal_status,
      tag_ids: [],
      alternative_names: [],
    )
    p[:alternative_names]&.reject!(&:blank?)
    p
  end
end
