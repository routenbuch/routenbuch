class VersionsController < ApplicationController
  before_action :set_base, only: [:index]
  skip_authorization_check :only => [:index]

  def index
    @q = @base_relation.ransack(params[:q])
    @versions = @q.result \
      .accessible_by(current_ability, :read) \
      .includes(:item, :user, :geo_ref, :route) \
      .reorder(created_at: :desc) \
      .page(params[:page])
  end

  private

  def set_base
    [
      Route,
      GeoRef,
      User,
      FirstAscentPerson,
      Closure
    ].each do |klass|
      param_name = "#{klass.table_name.singularize}_id"
      next unless params.key? param_name

      @base_object = klass.find(params[param_name])
      authorize! :read, @base_object

      @base_relation = case @base_object
                       when GeoRef
                         @base_object.self_and_descendent_versions
                       when Route
                         @base_object.related_versions
                       when User
                         raise CanCan::AccessDenied if @base_object != current_user

                         @base_object.authored_versions
                       else
                         @base_object.versions
                       end
      return
    end

    # global view
    @base_object, @base_relation = nil, Version
  end
end
