class FirstAscentPeopleController < ApplicationController
  before_action :set_base, only: %i[index]
  before_action :set_first_ascent_person, only: [:show, :edit, :update, :destroy]
  skip_authorization_check :only => [:index, :autocomplete]

  def autocomplete
    first, second = params[:term].split(/\s+/, 2)
    query = if second.nil?
              FirstAscentPerson.where(
                'first_name ILIKE :q OR last_name ILIKE :q',
                q: "%#{first}%"
              )
            else
              FirstAscentPerson.where(
                '(first_name ILIKE :first AND last_name ILIKE :second) OR (first_name ILIKE :second AND last_name ILIKE :first)',
                first: "%#{first}%",
                second: "%#{second}%"
              )
            end

    @first_ascent_people = query \
      .accessible_by(current_ability, :read) \
      .order(Arel.sql('LENGTH(first_ascent_people.first_name || first_ascent_people.last_name) ASC')) \
      .limit(5)

    render(
      json: @first_ascent_people.map do |f|
        {
          id: f.id,
          value: f.name,
          label: f.name
        }
      end
    )
  end

  def index
    @q = @base_relation.ransack(params[:q])
    @first_ascent_people = \
      @q \
      .result \
      .left_outer_joins(:routes) \
      .group('first_ascent_people.id') \
      .select('first_ascent_people.*, COUNT(routes.id) AS fa_count') \
      .reorder('fa_count DESC') \
      .accessible_by(current_ability, :read) \
      .page(params[:page])
  end

  def show
    authorize! :read, @first_ascent_person
  end

  def new
    @first_ascent_person = FirstAscentPerson.new
    authorize! :create, @first_ascent_person
  end

  def edit
    authorize! :update, @first_ascent_person
  end

  def create
    @first_ascent_person = FirstAscentPerson.new(first_ascent_person_params)
    authorize! :create, @first_ascent_person

    respond_to do |format|
      if @first_ascent_person.save
        format.html { redirect_to @first_ascent_person, notice: _('First ascent person was successfully created.') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    authorize! :update, @first_ascent_person
    respond_to do |format|
      if @first_ascent_person.update(first_ascent_person_params)
        format.html { redirect_to @first_ascent_person, notice: _('First ascent person was successfully updated.') }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    authorize! :destroy, @first_ascent_person
    @first_ascent_person.destroy
    respond_to do |format|
      format.html { redirect_to first_ascent_people_url, notice: _('First ascent person was successfully destroyed.') }
    end
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [geo_ref, geo_ref.first_ascent_people]
      else
        [nil, FirstAscentPerson]
      end
    authorize! :read, @base_object unless @base_object.nil?
  end

  def set_first_ascent_person
    @first_ascent_person = FirstAscentPerson.find(params[:id])
  end

  def first_ascent_person_params
    p = \
      params \
      .require(:first_ascent_person) \
      .permit(
        :first_name,
        :last_name,
        :dead,
        alternative_names: []
      )
    p[:alternative_names]&.reject!(&:blank?)
    p
  end
end
