class ProductsController < ApplicationController
  skip_authorization_check :only => [:autocomplete]

  def autocomplete
    @products = Product \
      .left_joins(:vendor) \
      .includes(:vendor) \
      .where(
        'products.name ILIKE (?) OR vendors.name ILIKE (?)',
        "%#{params[:term]}%",
        "%#{params[:term]}%"
      ) \
      .accessible_by(current_ability, :read) \
      .order('LENGTH(products.name) ASC, products.name ASC') \
      .limit(5)
    render(
      json: @products.map do |p|
        {
          id: p.id,
          value: p.name,
          label: [ p.vendor&.name, p.name ].reject(&:nil?).join(' - ')
        }
      end
    )
  end
end
