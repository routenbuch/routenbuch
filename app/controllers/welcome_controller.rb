class WelcomeController < ApplicationController
  skip_authorization_check only: [:index]

  def index
    if current_user&.home_geo_ref&.present?
      redirect_to geo_ref_path(current_user.home_geo_ref)
    elsif Routenbuch.config.key?('home_geo_ref_id')
      redirect_to geo_ref_path(Routenbuch.config['home_geo_ref_id'])
    else
      redirect_to world_geo_refs_path
    end
  end
end
