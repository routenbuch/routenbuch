class GradesController < ApplicationController
  skip_authorization_check only: :autocomplete

  def autocomplete
    @grades = \
      Grade.where('grade LIKE (?)', "%#{params[:term]}%") \
           .order('LENGTH(grades.grade) ASC') \
           .accessible_by(current_ability, :read)
    render(
      json: @grades.map do |g|
        {
          id: g.id,
          value: g.grade,
          label: "#{g.scale} #{g.grade}"
        }
      end
    )
  end
end
