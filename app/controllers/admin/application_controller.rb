class Admin::ApplicationController < ApplicationController
  before_action :enforce_admin_user
  def enforce_admin_user
    authorize! :access, :admin
  end

  private

  def navigation_context
    'admin'
  end

  def show_global_search
    false
  end
end
