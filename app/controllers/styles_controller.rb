class StylesController < ApplicationController
  skip_authorization_check :only => %i[autocomplete]

  def autocomplete
    filter = %i[belay_style sportive_style].map do |field|
      value = params[field]
      if value.blank?
        nil
      else
        [field, value]
      end
    end.reject(&:nil?).to_h

    @styles = Style \
      .where('name ILIKE (?)', "%#{params[:name]}%") \
      .where(**filter) \
      .order(priority: :desc) \
      .accessible_by(current_ability, :read)

    render json: @styles.map, only: %i[id name belay_style sportive_style redpoint]
  end
end
