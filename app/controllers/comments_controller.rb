class CommentsController < ApplicationController
  before_action :set_comment, only: %i[edit update destroy]
  before_action :set_base, only: %i[index new create]
  skip_authorization_check only: [:index]

  def index
    @q = @base_relation.ransack(params[:q])
    @comments = @q.result \
                  .includes(:user, geo_ref: :path_cache) \
                  .accessible_by(current_ability, :read) \
                  .order(**base_order) \
                  .page(params[:page])
  end

  def new
    @comment = Comment.new
    @comment.user = current_user
    @comment.target = @base_object
    authorize! :create, @comment
  end

  def edit
    authorize!(:update, @comment) unless can?(:update_kind, @comment)
  end

  def create
    @comment = Comment.new(comment_params)
    @comment.user = current_user
    @comment.target = @base_object
    @comment.geo_ref = if @base_object.is_a? GeoRef
                         @base_object
                       else
                         @base_object.geo_ref
                       end
    authorize! :create, @comment

    if @comment.save
      @comment.apply_tags
      redirect_to(
        polymorphic_path([@base_object.as_base_class, Comment]),
        notice: _('Comment was successfully created.')
      )
    else
      render :new
    end
  end

  def update
    authorize!(:update, @comment) unless can?(:update_kind, @comment)

    if @comment.update(update_params)
      redirect_to(
        polymorphic_path([@comment.target.as_base_class, Comment]),
        notice: _('Comment was successfully updated.')
      )
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @comment
    @comment.soft_delete!
    redirect_to(
      polymorphic_path([@comment.target.as_base_class, Comment]),
      notice: _('Comment was successfully removed.')
    )
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:route_id]
        route = Route.find(params[:route_id])
        [route, route.comments]
      elsif params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [geo_ref, geo_ref.self_and_descendent_comments]
      else
        [nil, Comment]
      end
    authorize! :read, @base_object unless @base_object.nil?
  end

  def base_order
    case @base_object
    when Route, GeoRef
      { pinned: :desc, created_at: :desc }
    else
      { created_at: :desc }
    end
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(
      :body,
      :kind,
      :private,
      :pinned
    )
  end

  def update_params
    if can? :update, @comment
      comment_params
    elsif can? :update_kind, @comment
      params.require(:comment).permit(:kind)
    else
      []
    end
  end
end
