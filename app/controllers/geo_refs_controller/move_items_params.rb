class GeoRefsController
  class MoveItemsParams
    include ActiveModel::Validations
    include ActiveModel::Conversion

    def initialize
      @routes = []
      @selected_routes = []
      @geo_refs = []
      @selected_geo_refs = []
    end

    attr_accessor :new_geo_ref, :geo_refs, :selected_geo_refs, :routes, :selected_routes

    validate :validate_selected_routes
    def validate_selected_routes
      selected_routes.each do |r|
        errors.add(
          :new_geo_ref,
          _('is a invalid destination for %{name}: %{error}') % {
            name: r.name,
            error: r.errors.messages.values.join(' ')
          }
        ) unless r.valid?
      end
    end

    validate :validate_selected_geo_refs
    def validate_selected_geo_refs
      selected_geo_refs.each do |g|
        errors.add(
          :new_geo_ref,
          _('is a invalid destination for %{name}: %{error}') % {
            name: g.name,
            error: g.errors.messages.values.join(' ')
          }
        ) unless g.valid?
      end
    end

    validates :new_geo_ref, presence: true
  end
end
