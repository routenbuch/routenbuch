class RouteLinksController < ApplicationController
  before_action :set_route_link, only: %i[edit update destroy show]
  before_action :set_route
  helper_method :kind_present?

  def new
    @route_link = RouteLink.new(
      route_from: @route,
      kind: params.dig(:route_link, :kind)
    )
    authorize! :create, @route_link
  end

  def edit
    authorize! :update, @route_link
  end

  def create
    @route_link = RouteLink.new(route_link_params)
    @route_link.route_from = @route
    authorize! :create, @route_link

    if @route_link.save
      redirect_to route_path(@route), notice: _('RouteLink was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @route_link

    if @route_link.update(route_link_params)
      redirect_to route_path(@route), notice: _('RouteLink was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @route_link
    @route_link.destroy
    redirect_to route_path(@route), notice: _('RouteLink was successfully removed.')
  end

  def show
    authorize! :read, @route_link
    redirect_to route_path(@route_link.route_from)
  end

  private

  def set_route_link
    @route_link = RouteLink.find(params[:id])
  end

  def set_route
    @route = Route.find(params[:route_id])
    authorize! :read, @route
  end

  def kind_present?
    params.dig(:route_link, :kind).present?
  end

  def route_link_params
    params \
      .require(:route_link) \
      .permit(
        :route_to_id,
        :description,
        :kind,
        :anchor_number,
        :first_anchor_number,
        :last_anchor_number
      )
  end
end
