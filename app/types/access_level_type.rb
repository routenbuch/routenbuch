class AccessLevelType < ActiveRecord::Type::Value
  class << self
    VALID_VALUES = [
      N_('confidential'),
      N_('private'),
      N_('internal'),
      *(N_('public') if Routenbuch.public?)
    ].freeze

    def valid_values
      VALID_VALUES
    end

    def is_valid?(value)
      VALID_VALUES.include?(value)
    end
  end

  def type
    :access_level
  end

  def cast(value)
    return nil if value.blank?
    return nil unless self.class.is_valid?(value)

    value
  end
end
