# frozen_string_literal: true
module SimpleNavigationBootstrap
  class CustomBootstrap < SimpleNavigation::Renderer::Base
    include BootstrapBase

    private

    def render_item(*args)
      SimpleNavigationBootstrap::CustomRenderedItem.new(*args).to_s
    end

    def bootstrap_version
      4
    end

    def container_class(_level)
      options.fetch(:navigation_class, ['nav'])
    end

    def prepare_name(name)
      return name unless name.is_a?(Hash)

      if name[:icon]
        icon_options = { class: "fa fa-#{name[:icon]} fa-fw", title: name[:title] }.reject { |_, v| v.nil? }
        content_tag(:i, '', icon_options) + ' ' + (name[:text] || '')
      else
        name[:text] || (raise SimpleNavigationBootstrap::Error::InvalidHash)
      end
    end
  end
end
