# == Schema Information
#
# Table name: first_ascent_people
#
#  id                :integer          not null, primary key
#  alternative_names :string           default([]), not null, is an Array
#  dead              :boolean          default(FALSE), not null
#  first_name        :string           default(""), not null
#  last_name         :string           default(""), not null
#  searchable_terms  :string           default(""), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
class FirstAscentPerson < ApplicationRecord
  has_many :routes, dependent: :nullify

  validates :last_name, presence: true

  def self.ransackable_attributes(_auth_object = nil)
    %w[first_name last_name]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end

  def self.ransackable_scopes(_auth_object = nil)
    %i[search_full_text]
  end

  def self.icon
    'flash'
  end

  def dead?
    dead
  end

  def name
    unless first_name.blank? && last_name.blank?
      "#{first_name} #{last_name} #{dead? ? '✝' : ''}"
    else
      _('no name')
    end
  end

  has_paper_trail(
    ignore: [:searchable_terms],
  )

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      searchable_terms:   'A',
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )

  before_save :update_searchable_terms
  def update_searchable_terms
    self.searchable_terms = ([first_name, last_name] + alternative_names).join(' ')
  end
end
