# == Schema Information
#
# Table name: geo_refs
#
#  id                :integer          not null, primary key
#  access            :string           default("internal"), not null
#  alternative_names :string           default([]), not null, is an Array
#  body              :text
#  description       :string
#  height            :integer
#  lat               :decimal(10, 6)
#  legal_status      :string
#  lng               :decimal(10, 6)
#  name              :string
#  orientation       :integer
#  priority          :integer          default(0), not null
#  searchable_terms  :string           default(""), not null
#  stats             :json
#  type              :string           default("Crag"), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  parent_id         :integer
#  zone_id           :integer
#
# Indexes
#
#  index_geo_refs_on_access        (access)
#  index_geo_refs_on_lat_and_lng   (lat,lng)
#  index_geo_refs_on_legal_status  (legal_status)
#  index_geo_refs_on_parent_id     (parent_id)
#  index_geo_refs_on_priority      (priority)
#  index_geo_refs_on_type          (type)
#  index_geo_refs_on_zone_id       (zone_id)
#
# Foreign Keys
#
#  fk_rails_...  (zone_id => zones.id)
#
class GeoRef < ApplicationRecord
  class << self
    GEO_REF_TYPES = %w[Country Region Crag Sector Parking].freeze

    def geo_ref_types
      GEO_REF_TYPES
    end

    def display_name
      N_('Location')
    end

    def icon
      'map-marker'
    end

    def valid_parent_classes
      []
    end

    def valid_parent_class?(klass)
      valid_parent_classes.include?(klass)
    end
  end

  has_many :childs, class_name: 'GeoRef', foreign_key: 'parent_id', dependent: :nullify
  belongs_to :parent, class_name: 'GeoRef', optional: true
  belongs_to :zone, optional: true
  has_many :secondary_zones, dependent: :destroy
  has_many :regulations, dependent: :destroy
  has_many :regulated_geo_refs, dependent: :destroy
  has_many :routes, dependent: :destroy
  has_many :photos, as: :target, dependent: :destroy
  has_many :topos, as: :target, dependent: :destroy
  has_many :comments, as: :target, dependent: :destroy
  has_many :paths, dependent: :destroy
  has_and_belongs_to_many :approaches, class_name: 'Path'

  has_many :closures, dependent: :destroy
  has_and_belongs_to_many :applicable_closures, class_name: 'Closure'

  has_one(
    :route_first,
    -> { select('DISTINCT ON (routes.geo_ref_id) routes.*').order(:geo_ref_id, :priority, :name) },
    class_name: 'Route'
  )
  has_one(
    :route_last,
    -> { select('DISTINCT ON (routes.geo_ref_id) routes.*').order(:geo_ref_id, priority: :desc, name: :desc) },
    class_name: 'Route'
  )
  has_many :related_versions, class_name: 'Version', dependent: :delete_all
  has_one :path_cache, class_name: 'GeoRefPathCache', dependent: :delete

  validates_presence_of :name

  validates(
    :orientation,
    numericality: {
      allow_nil: true,
      less_than: 361
    },
    if: :supports_orientation?
  )
  validates :orientation, absence: true, unless: :supports_orientation?

  validates(
    :height,
    numericality: {
      allow_nil: true,
      less_than: 5000
    },
    if: :supports_height?
  )
  validates :height, absence: true, unless: :supports_height?

  validates :zone, absence: true, unless: :supports_regulation?
  validates :secondary_zones, absence: true, unless: :supports_regulation?
  validates :legal_status, absence: true, unless: :supports_regulation?
  validates :legal_status, legal_status: true, if: :supports_regulation?
  attribute :legal_status, :legal_status

  validates(
    :type,
    inclusion: {
      in: GeoRef.geo_ref_types,
      message: '%{value} is not a valid GeoRef type!'
    }
  )

  validate :validate_parent_type
  # Checks if the location is allowed on this hierarchy level
  def validate_parent_type
    return if parent.nil? && is_a?(Country) 

    if parent.nil?
      errors.add(
        :parent,
        _('cannot be empty for type %{type}')
          .format(type: self.class.name)
      )
      return
    end

    # use type to allow change of STI type using becomes!
    myklass = type.constantize
    return if myklass.valid_parent_class?(parent.class)

    errors.add(
      :parent,
      _('must be of one of the following type: %{types}')
        .format(types: myklas.valid_parent_classes.join(', '))
    )
  end
  attr_accessor :skip_parent_changed_validation
  # Checks that the parent cannot be changed without being explicit about it
  validate :validate_parent_changed, unless: :skip_parent_changed_validation
  def validate_parent_changed
    if parent_id_changed? && self.persisted?
      errors.add(:parent, "cannot be changed")
    end
  end

  include AccessRestrictable
  include PermissionAssignable
  include Taggable
  include RbidIdentifiable

  has_paper_trail(
    ignore: %i[stats searchable_terms priority],
    meta: {
      geo_ref_id: :id
    }
  )

  scope :top_level, -> { where(parent: nil) }
  scope :with_location, -> { where.not(lat: nil).where.not(lng: nil) }
  scope :with_name, lambda { |name| where(name: name).or where('? = ANY(geo_refs.alternative_names)', name) }

  def self.ransackable_attributes(_auth_object = nil)
    %w[name type alternative_names body description height orientation legal_status]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[zone routes tags closures]
  end

  def self.ransackable_scopes(_auth_object = nil)
    %i[search_full_text]
  end

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      searchable_terms: 'A',
      description: 'B',
      body: 'C'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )

  # @return false
  def supports_orientation?
    false
  end

  # @return false
  def supports_height?
    false
  end

  # @return false
  def supports_regulation?
    false
  end

  def supports_ascents?
    Routenbuch.features.ascents?
  end

  def supports_ticklists?
    Routenbuch.features.ticklists?
  end

  # @return false
  def supports_paths?
    false
  end

  # @return false
  def supports_approaches?
    false
  end

  def supports_regulations?
    Regulation.valid_geo_ref_type? self.class
  end

  def supports_closures?
    Closure.valid_geo_ref_type? self.class
  end

  # Updates the stats field for this location
  def update_stats
    self.stats = {
      route_grade_stats: self.route_grade_stats,
      descendent_routes_count: self.descendent_routes.count,
      descendent_crags_count: self.descendent_crags.count,
      descendents_count: self.descendents.count,
      childs_count: self.childs.count,
    }
  end

  def route_grade_stats
    descendent_routes.joins(:grade)
      .group('grades.category')
      .reorder('MAX(grades.difficulty)')
      .count
  end

  def location?
    self.lat.present? and self.lng.present?
  end
  alias :latlng? :location?

  def location
    return unless latlng?

    Geokit::LatLng.new(lat,lng)
  end

  # search parents for closest location
  def next_location
    cur = self
    loop do
      break if cur.nil?
      return cur.location if cur.location.present?

      cur = cur.parent
    end

    return
  end

  def parents
    return GeoRef.none if parent_id.nil?

    GeoRef \
      .joins("INNER JOIN (#{parent_ids_sql}) parents ON geo_refs.id = parents.id") \
      .order('parents.path')
  end

  def descendents
    self_and_descendents.where.not(id: id)
  end

  def self_and_descendents
    GeoRef \
      .joins("INNER JOIN (#{descendant_ids_sql}) descentends ON geo_refs.id = descentends.id") \
      .order('descentends.priorities')
  end

  def ascents
    Ascent \
      .joins(:route) \
      .where('routes.geo_ref_id': self_and_descendents)
  end

  def ticklists
    Ticklist \
      .joins(:route) \
      .where('routes.geo_ref_id': self_and_descendents)
  end

  def first_ascent_people
    FirstAscentPerson \
      .distinct \
      .joins(:routes) \
      .joins("INNER JOIN (#{descendant_ids_sql}) descentends ON routes.geo_ref_id = descentends.id")
  end

  def descendent_routes
    Route \
      .joins("INNER JOIN (#{descendant_ids_sql}) descentends ON routes.geo_ref_id = descentends.id") \
      .order('descentends.priorities')
  end

  def descendent_crags
    self_and_descendents
      .where(type: 'Crag') - [self]
  end

  def self_and_descendent_photos
    Photo \
      .joins("INNER JOIN (#{descendant_ids_sql}) descentends ON photos.geo_ref_id = descentends.id") \
      .order('descentends.priorities')
  end

  def self_and_descendent_comments
    Comment \
      .joins("INNER JOIN (#{descendant_ids_sql}) descentends ON comments.geo_ref_id = descentends.id") \
      .order('descentends.priorities')
  end

  def self_and_descendent_regulations
    Regulation \
      .joins("INNER JOIN (#{descendant_ids_sql}) descentends ON regulations.geo_ref_id = descentends.id") \
      .order('descentends.priorities', :name)
  end

  def self_and_descendent_closures
    Closure \
      .joins("INNER JOIN (#{descendant_ids_sql}) descentends ON closures.geo_ref_id = descentends.id") \
      .order('descentends.priorities', :name)
  end

  def self_and_descendent_versions
    Version \
      .joins("INNER JOIN (#{descendant_ids_sql}) descentends ON versions.geo_ref_id = descentends.id") \
      .order('descentends.priorities')
  end

  # geokit
  acts_as_mappable default_units: :kms

  def update_self_and_parents_stats
    GeoRefUpdateStatsJob.perform_later(id)
  end

  before_save :update_searchable_terms
  def update_searchable_terms
    self.searchable_terms = ([name] + alternative_names).join(' ')
  end

  after_create :update_parent_stats
  after_destroy :update_parent_stats
  def update_parent_stats
    GeoRefUpdateStatsJob.perform_later(parent_id)
  end

  # return the crag itself or in case of sector the parent crag
  def parent_crag
    return self if instance_of? Crag
    return parent if instance_of? Sector

    raise ActiveRecord::RecordNotFound
  end

  private

  def parent_ids_sql
    table_name = GeoRef.table_name
    <<~SQL
      WITH RECURSIVE search_tree(id, parent_id, path) AS (
          SELECT id, parent_id, ARRAY[id]
          FROM #{table_name}
          WHERE id = #{parent_id}
        UNION ALL
          SELECT #{table_name}.id, #{table_name}.parent_id, path || #{table_name}.id
          FROM search_tree
          JOIN #{table_name} ON #{table_name}.id = search_tree.parent_id
          WHERE NOT #{table_name}.id = ANY(path)
      )
      SELECT id, path FROM search_tree
    SQL
  end

  def descendant_ids_sql
    table_name = GeoRef.table_name
    <<~SQL
      WITH RECURSIVE search_tree(id, path, priorities) AS (
          SELECT id, ARRAY[id], ARRAY[priority]
          FROM #{table_name}
          WHERE id = #{id}
        UNION ALL
          SELECT #{table_name}.id, path || #{table_name}.id, priorities || #{table_name}.priority
          FROM search_tree
          JOIN #{table_name} ON #{table_name}.parent_id = search_tree.id
          WHERE NOT #{table_name}.id = ANY(path)
      )
      SELECT id, path, priorities FROM search_tree
    SQL
  end
end
