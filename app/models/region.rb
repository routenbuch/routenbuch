# == Schema Information
#
# Table name: geo_refs
#
#  id                :integer          not null, primary key
#  access            :string           default("internal"), not null
#  alternative_names :string           default([]), not null, is an Array
#  body              :text
#  description       :string
#  height            :integer
#  lat               :decimal(10, 6)
#  legal_status      :string
#  lng               :decimal(10, 6)
#  name              :string
#  orientation       :integer
#  priority          :integer          default(0), not null
#  searchable_terms  :string           default(""), not null
#  stats             :json
#  type              :string           default("Crag"), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  parent_id         :integer
#  zone_id           :integer
#
# Indexes
#
#  index_geo_refs_on_access        (access)
#  index_geo_refs_on_lat_and_lng   (lat,lng)
#  index_geo_refs_on_legal_status  (legal_status)
#  index_geo_refs_on_parent_id     (parent_id)
#  index_geo_refs_on_priority      (priority)
#  index_geo_refs_on_type          (type)
#  index_geo_refs_on_zone_id       (zone_id)
#
# Foreign Keys
#
#  fk_rails_...  (zone_id => zones.id)
#
class Region < GeoRef
  class << self
    def valid_parent_classes
      [Country, Region]
    end

    def icon
      'map'
    end
  end
end
