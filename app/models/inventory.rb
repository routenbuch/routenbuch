# == Schema Information
#
# Table name: inventories
#
#  id            :bigint           not null, primary key
#  anchor_number :integer
#  description   :string
#  installed_at  :date
#  removed_at    :date
#  role          :string           default(""), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  product_id    :bigint
#  route_id      :bigint
#
# Indexes
#
#  index_inventories_on_product_id  (product_id)
#  index_inventories_on_route_id    (route_id)
#
class Inventory < ApplicationRecord
  class << self
    ROLES = [
      N_('belay'),
      N_('intermediate anchor')
    ].freeze

    def roles
      ROLES
    end

    def valid_role?(role)
      ROLES.include? role
    end

    def icon
      'database'
    end
  end

  belongs_to :route
  belongs_to :product
  delegate :vendor, to: :product

  validates(
    :anchor_number,
    presence: true,
    numericality: { only_integer: true }
  )

  validate :validate_role
  def validate_role
    return if role.blank?
    return if self.class.valid_role? role

    errors.add(:role, _('is not a valid role'))
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[anchor_number installed_at removed_at description role]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[product vendor route]
  end

  has_paper_trail(
    meta: {
      route_id: :route_id,
      geo_ref_id: proc { |i| i.route.geo_ref_id }
    }
  )
end
