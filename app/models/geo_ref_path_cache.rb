# == Schema Information
#
# Table name: geo_ref_path_caches
#
#  id         :bigint           not null, primary key
#  path       :jsonb
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  geo_ref_id :bigint           not null
#
# Indexes
#
#  index_geo_ref_path_caches_on_geo_ref_id  (geo_ref_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#
class GeoRefPathCache < ApplicationRecord
  belongs_to :geo_ref
end
