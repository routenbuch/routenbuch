# == Schema Information
#
# Table name: styles
#
#  id             :integer          not null, primary key
#  belay_style    :string           not null
#  name           :string
#  priority       :integer          default(50), not null
#  redpoint       :boolean          default(TRUE)
#  sportive_style :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_styles_on_name  (name) UNIQUE
#
class Style < ApplicationRecord
  class << self
    BELAY_STYLES = %w[
      lead
      second
      toprope
      boulder
      alternate_lead
      solo
      freesolo
      deepwatersolo
    ].freeze
    SPORTIVE_STYLES = %w[
      free
      flash
      onsight
      tainted
      unfished
      aid
    ].freeze

    def sportive_styles
      SPORTIVE_STYLES
    end

    def belay_styles
      BELAY_STYLES
    end
  end

  has_many :ascents

  validates :name, presence: true, uniqueness: true
  validates(
    :belay_style,
    inclusion: {
      in: Style.belay_styles,
      message: "%{value} is not a valid belay style"
    },
    presence: true
  )
  validates(
    :sportive_style,
    inclusion: {
      in: Style.sportive_styles,
      message: "%{value} is not a valid sportive style"
    },
    presence: true
  )

  def self.ransackable_attributes(_auth_object = nil)
    %w[name redpoint belay_style sportive_style]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
