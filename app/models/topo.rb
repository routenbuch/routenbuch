# == Schema Information
#
# Table name: topos
#
#  id          :integer          not null, primary key
#  description :string
#  target_type :string
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  target_id   :integer
#
# Indexes
#
#  index_topos_on_target_type_and_target_id  (target_type,target_id)
#
class Topo < ApplicationRecord
  validates_presence_of :title, :target
  belongs_to :target, polymorphic: true

  has_one_attached :background
  validates(
    :background,
    content_type: Routenbuch.supported_image_types,
    size: { less_than: Routenbuch.max_upload_size }
  )

  has_one_attached :overlay
  validates(
    :overlay,
    content_type: [:svg],
    size: { less_than: Routenbuch.max_upload_size }
  )

  def self.ransackable_attributes(_auth_object = nil)
    %w[title]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
