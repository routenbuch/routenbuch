# == Schema Information
#
# Table name: routes
#
#  id                     :integer          not null, primary key
#  alternative_names      :string           default([]), not null, is an Array
#  body                   :text
#  description            :string
#  first_ascent_day       :integer
#  first_ascent_month     :integer
#  first_ascent_year      :integer
#  legal_status           :string
#  name                   :string
#  pitches                :integer
#  priority               :integer          default(0), not null
#  searchable_terms       :string           default(""), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  first_ascent_person_id :integer
#  geo_ref_id             :integer
#  grade_id               :integer
#
# Indexes
#
#  index_routes_on_first_ascent_person_id  (first_ascent_person_id)
#  index_routes_on_geo_ref_id              (geo_ref_id)
#  index_routes_on_grade_id                (grade_id)
#  index_routes_on_legal_status            (legal_status)
#  index_routes_on_priority                (priority)
#
# Foreign Keys
#
#  fk_rails_...  (first_ascent_person_id => first_ascent_people.id)
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#  fk_rails_...  (grade_id => grades.id)
#
class Route < ApplicationRecord
  belongs_to :geo_ref
  delegate :effective_permissions, to: :geo_ref
  belongs_to :grade, optional: true
  belongs_to :first_ascent_person, optional: true
  has_many :ascents, dependent: :destroy
  has_many :ticklists, dependent: :destroy
  has_many :photos, as: :target, dependent: :destroy
  has_many :comments, as: :target, dependent: :destroy
  has_many :inventories, dependent: :destroy
  has_many :route_links_from, class_name: 'RouteLink', foreign_key: 'route_to_id', dependent: :destroy
  has_many :route_links_to, class_name: 'RouteLink', foreign_key: 'route_from_id', dependent: :destroy
  has_many :linked_routes_from, through: :route_links_from, source: 'route_from'
  has_many :linked_routes_to, through: :route_links_to, source: 'route_to'
  has_and_belongs_to_many :closures
  has_many :geo_ref_applicable_closures, through: :geo_ref, source: :applicable_closures
  has_many :related_versions, class_name: 'Version', dependent: :nullify

  validates :legal_status, legal_status: true
  attribute :legal_status, :legal_status

  def applicable_closures
    sub_query = [
      closures.select(:id),
      geo_ref_applicable_closures.select(:id)
    ].map(&:to_sql).join(' UNION ')
    Closure.distinct.where("closures.id IN (#{sub_query})")
  end

  validates_presence_of :name

  before_validation do
    # dont set on single pitch routes
    self.pitches = nil if self.pitches == 1
  end

  validate :validate_geo_ref_type
  def validate_geo_ref_type
    return if geo_ref.nil?
    return if geo_ref.is_a? Crag

    errors.add(:geo_ref, _('Routes can only be attached to a crag. (not to a %{type})') % {type: geo_ref.class.name})
  end

  validates(
    :first_ascent_year,
    numericality: {
      only_integer: true,
      greater_than: 1850,
      less_than: 2100
    },
    allow_nil: true
  )
  validates(
    :first_ascent_month,
    numericality: {
      only_integer: true,
      greater_than: 0,
      less_than: 13
    },
    allow_nil: true
  )
  validates(
    :first_ascent_day,
    numericality: {
      only_integer: true,
      greater_than: 0,
      less_than: 32
    },
    allow_nil: true
  )

  validate :validate_first_ascent_date
  def validate_first_ascent_date
    errors.add(:first_ascent_date, 'Year is missing.') \
      if first_ascent_month.present? && first_ascent_year.blank?

    errors.add(:first_ascent_date, 'Month is missing') \
      if first_ascent_day.present? && first_ascent_month.blank?

    if first_ascent_year.present? \
        && first_ascent_month.present? \
        && first_ascent_day.present?
      begin
        Date.new(first_ascent_year, first_ascent_month, first_ascent_day)
      rescue ArgumentError
        errors.add(:first_ascent_date, 'Invalid date') \
      end
    end
  end
  after_validation :copy_first_ascent_errors
  def copy_first_ascent_errors
    %i[
      first_ascent_year
      first_ascent_month
      first_ascent_day
    ].each do |field|
      errors[field].each do |error|
        errors.add(:first_ascent_date, error)
      end
    end
  end

  def first_ascent_date
    [first_ascent_year, first_ascent_month, first_ascent_day]
  end

  include Taggable
  include RbidIdentifiable
  include Rateable

  def self.ransackable_attributes(_auth_object = nil)
    %w[name body description first_ascent_year pitches legal_status]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[grade first_ascent_person geo_ref closures tags]
  end

  def self.ransackable_scopes(_auth_object = nil)
    %i[search_full_text]
  end

  has_paper_trail(
    ignore: [:searchable_terms, :priority],
    meta: {
      route_id: :id,
      geo_ref_id: :geo_ref_id
    }
  )

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      searchable_terms:   'A',
      description: 'B',
      body: 'C'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )

  def user_ascents(user)
    ascents.where(:user => user)
  end

  def user_has_redpoint(user)
    user_ascents(user).joins(:style).where(styles: { redpoint: true }).any?
  end

  def access
    geo_ref.access
  end

  after_commit do
    trigger_geo_ref_update_stats
  end

  attr_accessor :skip_geo_ref_update_stats
  def trigger_geo_ref_update_stats
    return if skip_geo_ref_update_stats
    return if saved_changes.except('priority', 'updated_at', 'searchable_terms').empty?

    geo_ref.update_self_and_parents_stats
  end

  before_create :set_initial_priority
  def set_initial_priority
    max = geo_ref.routes.maximum(:priority)
    self.priority = max.nil? ? 0 : (max+1)
  end

  def self.icon
    'book'
  end

  before_save :update_searchable_terms
  def update_searchable_terms
    self.searchable_terms = ([name] + alternative_names).join(' ')
  end
end
