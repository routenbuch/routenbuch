# == Schema Information
#
# Table name: zones
#
#  id           :integer          not null, primary key
#  body         :text
#  icon_path    :string           default(""), not null
#  name         :string
#  organization :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Zone < ApplicationRecord
  has_many :geo_refs
  has_many :secondary_zones

  validates :name, presence: true
  validates :icon_path, presence: true

  def self.ransackable_attributes(_auth_object = nil)
    %w[name organization]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
