# == Schema Information
#
# Table name: regulated_geo_refs
#
#  id            :bigint           not null, primary key
#  reason        :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  geo_ref_id    :bigint
#  regulation_id :bigint
#
# Indexes
#
#  index_regulated_geo_refs_on_geo_ref_id                    (geo_ref_id)
#  index_regulated_geo_refs_on_regulation_id                 (regulation_id)
#  index_regulated_geo_refs_on_regulation_id_and_geo_ref_id  (regulation_id,geo_ref_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#  fk_rails_...  (regulation_id => regulations.id)
#
class RegulatedGeoRef < ApplicationRecord
  belongs_to :regulation
  belongs_to :geo_ref
  has_many :inspections

  validate :validate_valid_geo_ref_type
  def validate_valid_geo_ref_type
    return if geo_ref.is_a? Crag

    errors.add(:geo_ref, _('Location must be a crag or sector'))
  end

  validates(
    :geo_ref,
    uniqueness: {
      scope: :regulation,
      message: _('can only be added once')
    }
  )

  def self.icon
    'map-marker'
  end
end
