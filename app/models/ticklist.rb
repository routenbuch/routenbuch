# == Schema Information
#
# Table name: ticklists
#
#  id         :integer          not null, primary key
#  comment    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  route_id   :integer
#  user_id    :integer
#
# Indexes
#
#  index_ticklists_on_route_id              (route_id)
#  index_ticklists_on_user_id               (user_id)
#  index_ticklists_on_user_id_and_route_id  (user_id,route_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (route_id => routes.id)
#  fk_rails_...  (user_id => users.id)
#
class Ticklist < ApplicationRecord
  validates :user, presence: true
  validates :route, presence: true, uniqueness: {
    scope: :user, message: "Route is already on your ticklist"
  }

  belongs_to :user
  belongs_to :route

  def self.icon
    'check-square-o'
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[comment]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[user route]
  end
end
