# == Schema Information
#
# Table name: permissions
#
#  id         :bigint           not null, primary key
#  level      :string           default("editor"), not null
#  geo_ref_id :bigint
#  user_id    :bigint
#
# Indexes
#
#  index_permissions_on_geo_ref_id              (geo_ref_id)
#  index_permissions_on_geo_ref_id_and_user_id  (geo_ref_id,user_id) UNIQUE
#  index_permissions_on_user_id                 (user_id)
#
class Permission < ApplicationRecord
  belongs_to :geo_ref
  belongs_to :user
  has_many :effective_permissions, dependent: :delete_all

  validates(
    :user,
    uniqueness: {
      scope: :geo_ref,
      message: _("cannot be assigned multiple times")
    }
  )

  def self.valid_levels
    %w[editor coordinator manager].freeze
  end

  validates :level, presence: true
  validate :validate_level
  def validate_level
    return if self.class.valid_levels.include? level

    errors.add(
      :level,
      _("must be one of %{levels}") % { levels: self.class.valid_levels.join(', ')}
    )
  end

  validate :validate_user
  def validate_user
    return if user.nil?
    return if user.role == 'contributor'

    errors.add(:user, _('must have the role contributor'))
  end

  def self.icon
    'lock'
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
