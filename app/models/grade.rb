# == Schema Information
#
# Table name: grades
#
#  id         :integer          not null, primary key
#  category   :string
#  difficulty :integer
#  grade      :string
#  scale      :string
#  scope      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Grade < ApplicationRecord
  validates :scale, :scope, :grade, :difficulty, presence: true
  validates :grade, length: {
    minimum: 1,
    maximum: 7,
  }
  validates :difficulty, numericality: { only_integer: true }

  def self.ransackable_attributes(auth_object = nil)
    %w[scale scope grade difficulty category]
  end

  def self.ransackable_associations(auth_object = nil)
    %w[]
  end

  def self.icon
    'tachometer'
  end

  def name
    grade
  end
end
