module TargetAccessFilterable
  extend ActiveSupport::Concern

  included do
    scope(
      :ids_of_geo_refs_with_access,
      lambda do |access_levels, user, permission_levels|
        query = select(:id) \
          .where(target_type: 'GeoRef') \
          .joins('JOIN geo_refs ON geo_refs.id = target_id') \
          .where('geo_refs.access': access_levels)
        unless user.nil? && permission_levels.nil?
          query = query \
            .joins('JOIN effective_permissions ON effective_permissions.geo_ref_id = geo_refs.id') \
            .joins('JOIN permissions ON effective_permissions.permission_id = permissions.id') \
            .where(
              'permissions.user_id': user.id,
              'permissions.level': permission_levels
            )
        end
        query
      end
    )

    scope(
      :ids_of_routes_with_access,
      lambda do |access_levels, user, permission_levels|
        query = select(:id) \
          .where(target_type: 'Route') \
          .joins('JOIN routes ON routes.id = target_id') \
          .joins('JOIN geo_refs ON geo_refs.id = routes.geo_ref_id') \
          .where('geo_refs.access': access_levels)
        query
      end
    )

    # select all comments by access level using the access
    # level of:
    # * geo_refs.access
    # * routes -> geo_refs.access
    scope(
      :with_access,
      lambda do |access_levels, user = nil, permission_level = nil|
        union_sql = [
          ids_of_geo_refs_with_access(access_levels, user, permission_level),
          ids_of_routes_with_access(access_levels, user, permission_level)
        ].map do |relation|
          relation.to_sql
        end.join(' UNION ')

        where("#{table_name}.id IN (#{union_sql})")
      end
    )
  end

  def with_permission?(user, *levels)
    if persisted?
      target.effective_permissions.where(user: user, level: levels).any?
    else
      target.effective_permissions.any? do |p|
        p.user_id == user.id && levels.include?(p.level) 
      end
    end
  end
end
