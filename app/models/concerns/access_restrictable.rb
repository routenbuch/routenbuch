module AccessRestrictable
  extend ActiveSupport::Concern

  included do
    validates :access, presence: true, access_level: true
    attribute :access, :access_level
  end

  def valid_access_levels
    AccessLevelType.valid_values
  end

  def is_valid_access_level?(value)
    valid_access_levels.include?(value)
  end
end
