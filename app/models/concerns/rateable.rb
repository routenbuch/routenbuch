module Rateable
  extend ActiveSupport::Concern

  included do
    has_one :rating, as: :target
  end

  def set_user_rating(user, score)
    create_rating! unless rating.present?

    user_rating = rating.user_ratings.find_by(user: user)
    if user_rating.present?
      user_rating.score = score
      user_rating.save!
    else
      UserRating.create!(
        rating: rating,
        user: user,
        score: score
      )
    end
  end

  def reset_user_rating(user)
    return unless rating.present?

    user_rating = rating.user_ratings.find_by(user: user)
    return if user_rating.nil?

    user_rating.destroy!
  end
end
