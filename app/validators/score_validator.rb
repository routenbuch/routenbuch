class ScoreValidator < ActiveModel::Validations::NumericalityValidator
  def options
    {
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 5,
      allow_nil: true
    }
  end
end
