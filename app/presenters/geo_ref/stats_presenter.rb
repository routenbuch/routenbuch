class GeoRef
  class StatsPresenter
    attr_accessor :relation

    def initialize(relation)
      @relation = relation
    end

    def routes_by_grade
      relation \
        .joins(routes: :grade) \
        .group('grades.difficulty') \
        .order('MAX(grades.difficulty)') \
        .pluck(Arel.sql("string_agg(distinct grades.grade, ',')"), 'COUNT(routes.id)')
    end

    def routes_by_first_ascent_person
      relation \
        .joins(routes: :first_ascent_person) \
        .group('first_ascent_people.id') \
        .pluck(Arel.sql("CONCAT(first_ascent_people.first_name, ' ', first_ascent_people.last_name)"), 'COUNT(routes.id)')
    end
  end
end
