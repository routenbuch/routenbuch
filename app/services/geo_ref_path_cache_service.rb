# service class for managing the path cache for GeoRefs
#
# @example Basic usage
#   GeoRefPathCacheService.update(
#     GeoRef.find_by(name: 'Frankenjura')
#   )
class GeoRefPathCacheService
  class << self
    # clear cache for a GeoRef
    # @param [GeoRef] geo_ref location to clear the cache
    def clear(geo_ref, recursive: true)
      if recursive
        GeoRefPathCache.where(geo_ref: geo_ref.self_and_descendents).delete_all
      else
        geo_ref&.path_cache&.delete!
      end
    end

    # update the cache for this GeoRef
    # @param [GeoRef] geo_ref location to update the cache for
    def update(geo_ref, recursive: true)
      GeoRefPathCache.transaction do
        current_path = geo_ref.parents.reverse.map do |parent|
          path_for(parent)
        end
        current_path << path_for(geo_ref)
        GeoRefPathCache.upsert(
          {
            geo_ref_id: geo_ref.id,
            path: current_path
          },
          unique_by: :geo_ref_id
        )

        recurse_update(current_path, geo_ref) if recursive
      end

      nil
    end

    private

    def recurse_update(parents_path, geo_ref)
      updates = []
      geo_ref.childs.each do |child|
        current_path = parents_path + [path_for(child)]
        recurse_update(current_path, child)
        updates << {
          geo_ref_id: child.id,
          path: current_path
        }
      end
      GeoRefPathCache.upsert_all(updates, unique_by: :geo_ref_id) if updates.any?
    end

    def path_for(geo_ref)
      {
        id: geo_ref.id,
        name: geo_ref.name,
        class: geo_ref.class.name
      }
    end
  end
end
