module Api
  module Embed
    module V1
      class RouteSerializer
        include JSONAPI::Serializer

        attributes :name, :description, :alternative_names
      end
    end
  end
end
