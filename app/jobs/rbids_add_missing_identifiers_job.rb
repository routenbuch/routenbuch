# add rbids to records that not yet have a id
class RbidsAddMissingIdentifiersJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    # rails may lazy load models
    # this force loads all model classes
    Rails.application.eager_load!

    # get all model classes that support rbid
    # and are base classes (exclude STI childs)
    rbid_classes = ApplicationRecord.descendants.select do |c|
      c.respond_to?(:without_rbid) && c.base_class?
    end

    rbid_classes.each do |klass|
      klass.without_rbid.each(&:create_rbid!)
    end
  end
end
