# update cached fields on closures
class ClosuresUpdateCachedAttributesJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Closure.all \
           .includes(:relevant_season_closures, :geo_ref) \
           .each do |c|
      c.save!
    end
  end
end
