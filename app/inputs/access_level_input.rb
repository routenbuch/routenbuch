class AccessLevelInput < SimpleForm::Inputs::Base
  def input(wrapper_options)

    @builder.select(
      attribute_name,
      access_levels.map { |l| [_(l), l] }.to_h,
      {},
      input_html_options
    )
  end

  def input_html_options
    super.merge({class: 'form-control'})
  end

  private

  def access_levels_by_ability
    ability = options[:ability]

    # preserve access - better way? clone, dup unset id :-/
    old_access = object.access

    levels = object_access_levels.select do |l|
      object.access = l
      ability.can? :update, object
    end

    # restore
    object.access = old_access

    levels
  end

  def access_levels
    if options.key? :ability
      access_levels_by_ability
    else
      object_access_levels
    end
  end

  def object_access_levels
    @builder.object.valid_access_levels
  end
end
