xml.channel do
  xml.title page_title
  xml.language I18n.locale.to_s
  if @base_object.present?
    description = _('Closures for %{region}') % { region: @base_object.name }
    link = polymorphic_url([@base_object.as_base_class, Closure])
  else
    description = _('All closures')
    link = closures_url
  end
  xml.description description
  xml.link link
  xml.atom :link, href: request.original_url, rel: 'self', type: 'application/rss+xml'

  @closures.each do |closure|
    xml.item do
      status = closure.active? ? _('closure active') : _('closure removed')
      xml.title "#{closure.name} (#{status})"
      xml.description render(
        partial: 'closures/feed_rss_description',
        formats: :html,
        locals: {
          item: closure,
          geo_refs: @geo_refs[closure.id],
          routes: @routes[closure.id],
        }
      )
      xml.pubDate closure.active_changed_at.rfc822
      xml.link closure_url(closure)
      xml.guid "#{closure_url(closure)}##{closure.active_changed_at}"
    end
  end
end
