How Do I Install Routenbuch?
============================

## Software Requirements

* Ruby 2.6+
* Postgres 10+
* Redis 5.0+

### Packages for building dependencies

* rubygem-bundler
* rubygem-rails
* rubygem-rake
* libvips-devel
* libpq-devel
* npm
* nodejs
* postgresql-server
* postgresql-contrib
* redis
* yarn

## Installing Prerequisites

Get the required nodejs depencies using:

    yarn install

To get the rubygems run:

    bundle config set path 'vendor/bundle'
    bundle install

## Compiling assets

    SECRET_KEY_BASE=0 rake assets:precompile

## Setting up rails secrets

    rake credentials:edit --environment production

## Database setup

Create a postgres user and grant the user the CREATEDB privilege:

`psql -U postgres`:

    CREATE USER routenbuch PASSWORD secret CREATEDB;

## Create a redis instance

You should setup a redis instance which can be reached over a unix socket.

## Configuration

Edit `config/routenbuch.yml` to change the settings for the appserver. The
database is configured in `config/database.yml` and the puma appserver in
`config/puma.yml`.

In production you should use a unix socket for puma.

## Setup the database

    rake db:setup

## Running the appserver with puma

    bundler exec puma --env production --config /path/to/routenbuch/config/puma.yml

## Running the worker process

    bundler exec sidekiq --env production --config /path/to/routenbuch/config/sidekiq.yml

## Setting up apache2

In your vhost add the following:

    DocumentRoot "/srv/www/webapps/routenbuch/public"

    <Directory "/srv/www/webapps/routenbuch/public">
        Options -Indexes +FollowSymLinks
        Require all granted
    </Directory>

    RequestHeader set "X-Forwarded-Proto" expr=%{REQUEST_SCHEME}
    RequestHeader set "X-Forwarded-SSL" expr=%{HTTPS}

    ProxyPass /favicon.ico !
    ProxyPass /robots.txt !
    ProxyPassMatch ^/(404|422|500).html$ !
    ProxyPass /assets/ !
    ProxyPass /packs/ !

    ProxyPass        / unix:///srv/www/webapps/routenbuch/tmp/sockets/puma.sock|http://localhost/
    ProxyPassReverse / unix:///srv/www/webapps/routenbuch/tmp/sockets/puma.sock|http://localhost/
