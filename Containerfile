FROM ruby:3.3
ENV LANG en_US.UTF-8
ENV RAILS_ENV production
ENV NODE_PATH /usr/src/node_modules
ENV WEBPACKER_NODE_MODULES_BIN_PATH /usr/src/node_modules/.bin
ARG BUNDLE_GEMFILE
ENV BUNDLE_GEMFILE=${BUNDLE_GEMFILE:-Gemfile}
ENV BOOTSNAP_CACHE_DIR /cache/bootsnap
ENV RUBY_YJIT_ENABLE 1

RUN ["/bin/bash", "-o", "pipefail", "-c", "curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -"]
RUN ["/bin/bash", "-o", "pipefail", "-c", "curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -"]
RUN ["/bin/bash", "-o", "pipefail", "-c", "echo 'deb https://dl.yarnpkg.com/debian/ stable main' | tee /etc/apt/sources.list.d/yarn.list"]

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  locales \
  postgresql-client \
  curl \
  neovim \
  nodejs \
  apt-transport-https \
  libvips \
  graphviz \
  gosu \
  yarn \
  && rm -rf /var/lib/apt/lists/* \
  \
  && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen \
  && /usr/sbin/update-locale en_US.UTF-8 \
  \
  && mkdir /usr/src/app

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN echo '--modules-folder "/usr/src/node_modules"' > .yarnrc \
  && yarn install --frozen-lockfile --production=false \
  && yarn cache clean \
  && yarn check

COPY Gemfile* ./
RUN bundle install 

COPY . .

RUN groupadd --system app \
  && useradd --system \
  --comment "Routenbuch application" \
  --create-home \
  -g app \
  app \
  && mkdir -p /data/storage /data/tmp /cache/bootsnap \
  && ln -s /data/storage /usr/src/app/storage \
  && rm -rf /usr/src/app/tmp \
  && ln -s /data/tmp /usr/src/app/tmp
RUN SECRET_KEY_BASE=0 NODE_ENV=development rake assets:precompile
RUN bundle exec bootsnap precompile --gemfile app/ lib/ \
  && chown -R app:app /data /cache

VOLUME [ "/data", "/cache" ]

ENTRYPOINT ["container/entrypoint.sh"]
CMD []

EXPOSE 3000

# Doesn't exist in OCI container format
# HEALTHCHECK --interval=1m --timeout=60s --retries=5 \
#   CMD curl -f http://localhost:3000/health_check || exit 1
