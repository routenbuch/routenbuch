reasons = [
  N_('breeding birds'),
  N_('bats'),
  N_('protected plants'),
  N_('administrative closure'),
  N_('legal regulations'),
]
reasons.each do |name|
  ClosureReason.find_or_create_by!(name: name)
end
