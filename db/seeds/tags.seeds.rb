
# category, name, description
tags = [
  [ 'Route', 'bolts', 'glue-in bolts', N_('Equipped with glue-in bolts') ],
  [ 'Route', 'bolts', 'expansion bolts', N_('Equipped with expansion bolts') ],
  [ 'Route', 'bolts', 'pitons', N_('Equipped with pitons') ],
  [ 'Route', 'bolts', 'self-made', N_('Equipped with self-made bolts') ],

  [ 'Route', 'protection', 'no protection', N_('No protection') ],
  [ 'Route', 'protection', 'bad protection', N_('Bad protection') ],
  [ 'Route', 'protection', 'good protection', N_('Good protection') ],
  [ 'Route', 'protection', 'very good protection', N_('Very good protection') ],
  [ 'Route', 'protection', 'nuts', N_('Use of nuts required or recommended') ],
  [ 'Route', 'protection', 'cams', N_('Use of cams required or recommended') ],
  [ 'Route', 'protection', 'slings', N_('Use of slings required or recommended') ],

  [ 'Crag', 'location', 'deciduous forest', N_('located in deciduous woods') ],
  [ 'Crag', 'location', 'coniferous forest', N_('located in coniferous woods') ],
  [ 'Crag', 'location', 'meadow', N_('located in the meadow') ],
  [ 'Crag', 'location', 'cave', N_('location is a cave') ],

  [ 'Crag', 'children', 'children safe', N_('foot of the crag is safe for children') ],
  [ 'Crag', 'children', 'children dangerous', N_('foot of the crag is dangerous for children') ],

  [ 'Crag', 'inclination', 'inclined', N_('inclined area') ],
  [ 'Crag', 'inclination', 'vertical', N_('vertical area') ],
  [ 'Crag', 'inclination', 'slightly overhanging', N_('slightly overhanging area') ],
  [ 'Crag', 'inclination', 'overhanging', N_('overhanging area') ],
  [ 'Crag', 'inclination', 'roof', N_('highly overhanging area or roof') ],
  [ 'Crag', 'inclination', 'crack', N_('Crack climbing') ],
  [ 'Crag', 'inclination', 'corner', N_('Climing on a corner') ],
  [ 'Crag', 'inclination', 'dihedral', N_('Climbing in a dihedral') ],
  [ 'Crag', 'inclination', 'slab', N_('Slab climbing') ],

  [ 'Crag', 'exposure', 'sunny', N_('all day in the sun') ],
  [ 'Crag', 'exposure', 'sunny morning', N_('sun in the morning') ],
  [ 'Crag', 'exposure', 'sunny afternoon', N_('sun in the afternoon') ],
  [ 'Crag', 'exposure', 'no sun', N_('no sun') ],
  [ 'Crag', 'exposure', 'rain safe', N_('climbable in the rain') ],
  [ 'Crag', 'exposure', 'dry fast', N_('gets dry fast') ],

  [ 'Crag', 'legal', 'national park', N_('located in a national park') ],
  [ 'Crag', 'legal', 'nature park', N_('located in a nature park') ],
  [ 'Crag', 'legal', 'natural reserve', N_('located in a natural reserve') ],
  [ 'Crag', 'legal', 'protected landscape', N_('located in protected landscape') ],
  [ 'Crag', 'legal', 'natural monument', N_('is a natural monument') ],
  [ 'Crag', 'legal', 'cultural monument', N_('is a cultural monument') ],
  [ 'Crag', 'legal', 'ffh area', N_('located in FFH protected area') ],
  [ 'Crag', 'legal', 'spa area', N_('located in SPA protected area') ],

  [ 'Route', 'style', 'technical', N_('Climbing requires technical skills') ],
  [ 'Route', 'style', 'power', N_('Climbing requires maximum power') ],
  [ 'Route', 'style', 'endurance', N_('Climbing requires endurance') ],
  [ 'Route', 'style', 'finger strength', N_('Climbing requires finger strength') ],
  [ 'Route', 'style', 'flexibility', N_('Climbing requires flexibility') ],
  [ 'Route', 'style', 'tricky', N_('Requires a special or tricky move') ],
  [ 'Route', 'style', 'crux', N_('Route has a crux') ],

  [ 'Route', 'first ascent', 'fa top', N_('First ascent from the top') ],
  [ 'Route', 'first ascent', 'fa bottom', N_('First ascent from the bottom') ],

  [ 'Route', 'other', 'maintenance required', N_('maintenance is required') ],
  [ 'Crag', 'other', 'maintenance required', N_('maintenance is required') ],
  [ 'Crag', 'other', 'residents', N_('located near town or site') ],
  [ 'Route', 'other', 'chipped', N_('Chipped holds') ],
  [ 'Route', 'other', 'summit book', N_('Equipped with a summit book') ],
  [ 'Crag', 'other', 'summit book', N_('Equipped with a summit book') ],
]

tags.each do |tag|
  begin
    Tag.create!(model: tag[0], category: tag[1], name: tag[2], description: tag[3])
  rescue ActiveRecord::RecordInvalid => e
    raise e unless e.message =~ /Name tags can only exist once per model/
  end
end
