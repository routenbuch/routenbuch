(0..50).each do |country|
  c = Country.create!(
    name: "Country #{country}"
  )
  (0..20).each do |region|
    r = Region.create!(
      name: "Region #{region}",
      parent: c
    )
    (0..20).each do |subregion|
      sr = Region.create!(
        name: "Subregion #{region}-#{subregion}",
        parent: r
      )
      (0..40).each do |crag|
        Crag.create!(
          name: "Crag #{region}-#{subregion}-#{crag}",
          parent: sr
        )
      end
    end
  end
end
