class AddClosureToGeoRef < ActiveRecord::Migration[6.0]
  def change
    add_reference :geo_refs, :closure, foreign_key: true, null: true
  end
end
