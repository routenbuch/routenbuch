class AddStickyToComments < ActiveRecord::Migration[6.0]
  def change
    add_column :comments, :sticky, :boolean, null: false, default: false
    add_index :comments, :sticky
  end
end
