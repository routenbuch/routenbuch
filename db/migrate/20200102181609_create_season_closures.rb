class CreateSeasonClosures < ActiveRecord::Migration[6.0]
  def change
    create_table :season_closures do |t|
      t.references :closure, null: false, foreign_key: true
      t.string :description
      t.integer :year, null: false
      t.integer :start_month, null: true
      t.integer :start_day_of_month, null: true
      t.integer :end_month, null: true
      t.integer :end_day_of_month, null: true

      t.timestamps

			t.index %i[closure_id year], unique: true
    end
  end
end
