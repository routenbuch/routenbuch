class CreateTags < ActiveRecord::Migration[5.1]
  def change
    create_table :tags do |t|
      t.string :name
      t.string :description
      t.boolean :public

      t.timestamps
    end
    add_index :tags, :name
    add_index :tags, :public

    create_table :assigned_tags, id: false do |t|
      t.belongs_to :taggable, index: true, polymorphic: true
      t.belongs_to :tag, index: true
    end
  end
end
