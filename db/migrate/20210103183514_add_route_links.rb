class AddRouteLinks < ActiveRecord::Migration[6.1]
  def change
    create_table :route_links do |t|
      t.references :route_from, foreign_key: {to_table: :routes}
      t.references :route_to, foreign_key: {to_table: :routes}
      t.string :description
      t.string :kind, null: false, default: 'belay'
      t.integer :anchor_number
      t.integer :first_anchor_number
      t.integer :last_anchor_number
      t.timestamps
    end
  end
end
