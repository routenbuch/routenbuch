class ExtendTags < ActiveRecord::Migration[6.0]
  def change
    add_column :tags, :model, :string, null: false, default: 'Crag'
    add_column :tags, :icon, :string, null: true
    add_index :tags, :model
    add_index :tags, [:model, :name], unique: true
  end
end
