class AddRedpointToStyle < ActiveRecord::Migration[5.0]
  def change
    add_column :styles, :redpoint, :boolean, :default => false
  end
end
