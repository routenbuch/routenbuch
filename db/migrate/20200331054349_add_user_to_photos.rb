class AddUserToPhotos < ActiveRecord::Migration[6.0]
  def change
    rename_column :comments, :sticky, :pinned

    add_column :photos, :private, :boolean, null: false, default: false
    add_index :photos, :private

    add_column :photos, :pinned, :boolean, null: false, default: false
    add_index :photos, :pinned

    add_column :photos, :cover_photo, :boolean, null: false, default: false
    add_index :photos, :cover_photo

    add_reference :photos, :user, foreign_key: true
    first_admin = User.where(role: 'admin').first
    Photo.update_all(user_id: first_admin.id)
    change_column_null(:photos, :user_id, false)
  end
end
