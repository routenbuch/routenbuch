class AddRegulationToGeoRef < ActiveRecord::Migration[5.0]
  def change
    add_reference :geo_refs, :regulation, foreign_key: true
  end
end
