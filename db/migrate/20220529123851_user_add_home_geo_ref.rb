class UserAddHomeGeoRef < ActiveRecord::Migration[6.1]
  def change
    add_reference :users, :home_geo_ref, foreign_key: { to_table: :geo_refs }
  end
end
