module Routenbuch
  class Identifier
    class IdentifierInvalid < StandardError ; end

    class << self
      ALPHABET = 'SauTAnz9m1NU8G5DHrxpqZFMKd7CgbYeEstoQwPR4yLicJj2Xvkf3BW6Vh'
      DIGITS = ALPHABET.split('')
      DIGITS_INDEX = DIGITS.each_with_index.map { |d, i| [d, i] }.to_h
      BASE = ALPHABET.length

      def encode(value)
        value.digits(58).map { |i| DIGITS[i] }.reverse.join
      end

      def decode(value)
        result = 0
        value.split('').reverse.each_with_index do |char, index|
          char_index = DIGITS_INDEX[char]
          raise IdentifierInvalid, 'Is not a valid identifier' \
            if char_index.nil?

          result += (BASE**index) * char_index
        end

        result
      end
    end
  end
end
