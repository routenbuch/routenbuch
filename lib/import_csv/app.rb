require 'optparse'

module ImportCsv
  class App
    def initialize(handle = nil)
      @handle = handle || ARGF
      @format = 'frankenjura'
      @mode = 'import'
      parse_opts
    end

    def parse_opts
      OptionParser.new do |opts|
        opts.banner = "Usage: import_csv [options] [file]"

        opts.on("-pNAME", "--parent_name=NAME", "Specify parent node for import data") do |parent_name|
          @parent = GeoRef.find_by(name: parent_name)
          if @parent.nil?
            puts "Cant find parent GeoRef with name #{parent_name}!"
            puts opts
            exit
          end
        end

        opts.on("-fFORMAT", "--format=FORMAT", "Specify CSV row format for import data") do |format|
          @format = format
          unless %w[frankenjura regensburg].include? format
            puts "Invalid row format #{format}!"
            puts opts
            exit
          end
        end

        opts.on("-mMODE", "--mode=MODE", "operation mode to execute") do |mode|
          @mode = mode
          unless %w[import fix-route-fa].include? mode
            puts "Invalid operation mode #{mode}!"
            puts opts
            exit
          end
        end

        opts.on("-lLEVEL", "--log_level=LEVEL", "Rails logger log_level") do |level|
          Rails.logger.level = level.to_sym
        end
      end.parse!
    end

    def run
      puts "running in rails env: #{Rails.env}"

      @handle.each_line.with_index do |line, line_number|
        next if line_number == 0
        next if line =~ /^\s*$/
        next if line =~ /^\#/

        record = parse_line(line)
        puts "#{line_number}: reading #{record[:region]} / #{record[:route]}..."

        begin
          case @mode
          when 'import'
            import_record(record)
          when 'fix-route-fa'
            fix_route_fa(record)
          end
        rescue StandardError => e
          raise "failed to import line #{line_number + 1} (#{record[:route]}): #{e}"
        end
      end
    end

    def import_record(record)
      region = get_region(record[:region])
      zone = get_zone(record[:zone])
      height = convert_height(record[:crag_height])
      orientation = convert_orientation(record[:crag_orientation])
      latlng = convert_latlng(record[:latlng])
      crag = get_crag(record[:crag], region, zone, height, orientation, latlng)

      unless record[:route].blank?
        fa_person = get_fa_person(record[:fa_person])
        fa_year = convert_fa_year(record[:fa_year])
        grade = get_grade(record[:grade])
        get_route(
          record[:route],
          crag,
          fa_person,
          fa_year,
          grade,
        )
      end
    end

    def fix_route_fa(record)
      return if record[:route].blank?
      return if record[:fa_year].blank?

      by_name = Route.where(name: record[:route])
      by_name_and_crag = Route.joins(:geo_ref).where(name: record[:route], 'geo_refs.name' => record[:crag])

      route = if by_name.count == 1
                by_name.first
              elsif by_name_and_crag.count == 1
                by_name_and_crag.first
              end

      unless route.present?
        STDERR.puts "cannot find route: #{record[:crag]}/#{record[:route]}"
        return
      end

      if route.first_ascent_year.present?
        puts "first_ascent_year is already present"
        return
      end

      year = convert_fa_year(record[:fa_year])
      puts "settings first_ascent_year #{year}"
      route.update!(
        first_ascent_year: year
      )
    end

    def parse_line(line)
      fields = line.split('|')
      send("row_format_#{@format}", fields)
    end

    def row_format_frankenjura(fields)
      {
        region: fields[0],
        crag: fields[1],
        route: fields[2],
        grade: fields[3],
        stars: fields[4],
        fa_person: fields[5],
        fa_year: fields[6],
        crag_height: fields[7],
        crag_orientation: fields[8],
        zone: fields[9]
      }
    end

    def row_format_regensburg(fields)
      {
        region: fields[0],
        crag: fields[1],
        latlng: fields[2],
        zone: fields[3]
      }
    end

    def get_region(name)
      Region.find_or_create_by!(
        name: name,
        parent: @parent
      )
    end

    def get_crag(name, region, zone, height, orientation, latlng)
      Crag.find_or_create_by!(
        name: name, 
        parent: region
      ) do |crag|
        crag.zone = zone
        crag.height = height
        crag.orientation = orientation
        if latlng.is_a? Array
          crag.lat = latlng[0]
          crag.lng = latlng[1]
        end
      end
    end

    def get_zone(name)
      return if name.blank?

      name.gsub!(/Zone(\d+)/, 'Zone \1')
      name.gsub!(/^(\d+)$/, 'Zone \1')
      Zone.find_by!(name: name)
    rescue ActiveRecord::RecordNotFound
      raise "Can't find zone #{name}"
    end

    def convert_height(height)
      return if height.blank?

      height.to_i
    end

    def convert_orientation(orientation)
      return if orientation.nil?

      case orientation
      when 'Nord' then 0
      when 'Nordost', 'Nord, Ost' then 45
      when 'Ost' then 90
      when 'Südost', 'Süd, Ost' then 135
      when 'Süd' then 180
      when 'Südwest', 'Süd, West' then 225
      when 'West' then 270
      when 'Nordwest', 'Nord, West' then 315
      when 'alle'
        nil
      else
        raise "Unknown orientation #{orientation}"
      end
    end

    def get_fa_person(name)
      return if name.nil?
      return if name =~ /^\s*$/

      if name =~ /\s/
        firstname, lastname = name.split(/\s+/, 2)
      else
        firstname = ''
        lastname = name
      end

      FirstAscentPerson.find_or_create_by!(
        first_name: firstname,
        last_name: lastname
      )
    end

    def get_grade(name)
      return if name.nil?

      # aid grades
      if(match = /([aA][0-6])/.match(name))
        return Grade.find_by!(
          scope: 'aid_climbing',
          scale: 'traditional',
          grade: match[1].upcase
        )
      end

      # mixed grades
      if name =~ /^[^\/]+\/[^\/]+$/
        # if mixed grade exists
        grade = Grade.find_by(
          scope: 'free_climbing',
          scale: 'UIAA',
          grade: name
        )
        return grade unless grade.nil?

        # otherwise remove second part of mixed grade 8/8+ -> 8
        name.gsub!(/\/.*$/, '')
      end

      # remove +/- for 1-4
      name.gsub!(/^([1-4])[+-]$/, '\1')
      Grade.find_by!(
        scope: 'free_climbing',
        scale: 'UIAA',
        grade: name
      )
    end

    def convert_fa_year(year)
      year = year.to_i
      case year
      when 1900..2100
        year
      when 0..99
        1900 + year
      end
    end

    def convert_latlng(latlng)
      return if latlng.blank?

      coords = latlng.scan(/(\d+)[°\.](\d+)\.(\d+)´/).map do |d,m,s|
        d.to_f + m.to_f/60 + s.to_f/3600
      end
      raise "cant parse coordinates: #{latlng}" unless coords.size == 2

      coords
    end

    def get_route(name, crag, fa_person, fa_year, grade)
      Route.find_or_create_by!(
        name: name,
        geo_ref: crag,
      ) do |route|
        route.first_ascent_person = fa_person
        route.first_ascent_year = fa_year
        route.grade = grade
      end
    end
  end
end
