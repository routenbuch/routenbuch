## Setting up a person development environment

This document describes how to get started with a personal
development environment using docker on linux.

### Prerequisites

* Linux OS (eg. Debian, Ubuntu, etc.)
* Git client
* Podman or Docker
* podman-compose or docker-compose 

For Podman you need the following packages:
* podman
* podman-plugins or cni-plugin-dnsname
* podman-compose
* slirp4netns

For Docker you need the following packages:
* docker or docker-ce
* docker-compose

### Checkout routenbuch sources

To get the routenbuch source clone the respository to a location of your choice.

```sh
git clone https://gitlab.com/routenbuch/routenbuch.git ~/development/routenbuch
cd ~/development/routenbuch
```

### Start development instance

To startup development containers you can either use 'podman compose':

```sh
podman compose -p routenbuch up
```

or docker-compose:

```sh
docker-compose up
```

On first startup it will take some time to build the containers from the `Containerfile`.

After the images have been build the following containers will be started:

* **web** - rails webserver (puma)
* **webpacker** - webpacker server for dynamic assets
* **worker** - sidekiq background job worker
* **redis** - redis database for sidekiq communication
* **database** - postgres database server

After startup the application can be access by browser:

* [Open browser on localhost:3000](http://localhost:3000)

On first startup the database will be empty and the webserver will display a `ActiveRecord::PendingMigrationError` exeption
as the database is still empty at this stage.

### Initialize database

To generate the database tables and initialize it with some basic records execute the `db:setup` action inside the `web` container.

```sh
podman compose -p routenbuch exec web rails db:setup
# or
docker-compose exec web rails db:setup
```

After refreshing the browser the routenbuch instance should be ready.

### Create initial admin user

To be able to login you must create a user first.

This can be done using the rails console:

```sh
docker-compose exec web rails console
or
podman compose -p routenbuch exec web rails console
```

The rails console is a irb (interactive ruby) shell with the rails environment loaded.

To create an admin user use the following code:

```ruby
User.create!(email: 'root@localhost', password: 'secret', role: 'admin')
```

Use an email address and password of your choice.

Log in and have fun!
